﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PuppeteerSharp;

namespace PuppeteerDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            CDPSession session = new CDPSession();
            session.Connect();

            Console.WriteLine("ok");
            Console.ReadKey();
            */

            
            var d = Task.Run(async () =>
            {
                await Executed();
            });
            d.Wait();

        }

        async static Task Executed()
        {
            var options = new LaunchOptions { Headless = false, ExecutablePath = "C:/Program Files/Google/Chrome/Application/chrome.exe" };

            var browser = await Puppeteer.LaunchAsync(options);
            
            var page = await browser.NewPageAsync();
            await page.GoToAsync("file:///D:/laiye/projects/uibot-chrome/testpage/index.html");
            // await page.EvaluateExpressionAsync("localStorage.demo='puppteer'");

            var localStorage = await page.EvaluateFunctionAsync<Dictionary<string, string>>("async () => Object.assign({}, window.localStorage)");

            Console.WriteLine(localStorage);
            Console.ReadKey();
        }
    }
}
