﻿; (function (g) {
    var initUiBot = function (g) {
        if (g.UiBotInstance) {
            return g.UiBotInstance;
        }
        g.UiBotInstance = {};
        var d = g.document,
            am = d.createElement('script'),
            h = d.head || d.getElementsByTagName('head')[0],
            aex = {
                'id': 'UiBotContainerPlaceholder',
                'data-cfasync': 'false',
                'async': 'true',
                'defer': 'true',
                'charset': 'utf-8'
            };
        for (var attr in aex) {
            am.setAttribute(attr, aex[attr]);
        }
        h.appendChild(am);
        d.addEventListener('click', function (evt) {
            var targ = evt.target || evt.srcElement || evt.originalTarget;
            var elText = targ.value || targ.nodeName || targ.name || targ.textContent || targ.innerText;
            console.log(elText);
            // alert(elText);
        });
        // inject into iframe
        d.querySelectorAll('iframe').forEach(function (frame) {
            if (frame.contentWindow) {
                setTimeout(function () {
                    initUiBot(frame.contentWindow, 'inner iframe');
                }, 2000);
            }
        });
    }
    initUiBot(g);
    return g.UiBotInstance;
})(window);