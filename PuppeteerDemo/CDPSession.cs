﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PuppeteerSharp;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Reflection;
using System.IO;
using Microsoft.Extensions.Logging;
using PuppeteerSharp.Transport;
using System.Threading;

namespace PuppeteerDemo
{
    class CDPSession
    {
        private const string V = "disable-web-security";

        private static ILoggerFactory GetLoggerFactory()
        {
            var factory = new LoggerFactory();

            var appDataDir = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            var logDir = Path.Combine(appDataDir, "CDP", "Logs");
            if (!Directory.Exists(logDir))
            {
                Directory.CreateDirectory(logDir);
            }


            var logPath = Path.Combine(logDir, $"CDP.log");
            Console.WriteLine("logPath..." + logPath);
            factory.AddFile(logPath, LogLevel.Trace);

            return factory;
        }



        public async void Connect()
        {
            var client = new HttpClient();
            
            client.BaseAddress = new Uri("http://localhost:38890/");
            client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));

            var url = "json/version";
            HttpResponseMessage response = await client.GetAsync(url);
            response.EnsureSuccessStatusCode();
            var resp = await response.Content.ReadAsStringAsync();
            Console.WriteLine(resp);
            var jsonResult = JObject.Parse(resp);
            var webSocketDebuggerUrl = jsonResult.Value<string>("webSocketDebuggerUrl");
            var options = new ConnectOptions()
            {
                BrowserWSEndpoint = webSocketDebuggerUrl
            };
            

            var browser = await PuppeteerSharp.Puppeteer.ConnectAsync(options, GetLoggerFactory());
            
            /*
            var opt = new LaunchOptions()
            {
                Args = new[]
                {
                    "--disable-web-security",
                    "--disable-features=IsolateOrigins,site-per-process",
                },
                Headless = false
            };
            var browser = await Puppeteer.LaunchAsync(opt);
            var pp = await browser.NewPageAsync();
            await pp.GoToAsync("http://localhost:1234/");
            */
            var context = browser.BrowserContexts()[0];
            
            try
            {
                var pages = await browser.PagesAsync();
                string injectContent = LoadEmbedScript("init.js");
                foreach (Page page in pages)
                {
                    var session = await page.Target.CreateCDPSessionAsync();

                    session.MessageReceived += (_, e) =>
                    {
                        Console.WriteLine(e);
                    };


                    if (page != null)
                    {
                        page.Dialog += async (sender, e) =>
                        {
                            // await e.Dialog.Dismiss();
                        };
                        await page.EvaluateExpressionAsync(injectContent);
                        page.Load += async (p, b) =>
                        {
                            await ((Page)p).EvaluateExpressionAsync(injectContent);
                            Console.WriteLine("same page inject js..."+ ((Page)p).Url);
                            foreach (Frame frame in page.Frames)
                            {
                                Console.WriteLine("new frame url1..." + frame.Url);
                                await frame.EvaluateExpressionAsync(injectContent);
                                var t = await frame.GetTitleAsync();
                                Console.WriteLine("new frame title1..." + t);
                            }
                        };
                        foreach (Frame frame in page.Frames)
                        {
                            Console.WriteLine("new frame url..." + frame.Url);
                            await frame.EvaluateExpressionAsync(injectContent);
                            var t = await frame.GetTitleAsync();
                            Console.WriteLine("new frame title..." + t);
                            // var webSocket = await WebSocketTransport.DefaultWebSocketFactory(new Uri("ws://localhost:38890/devtools/page/9892BBD834492B89C56C9059C4AD88FB"), null, CancellationToken.None);
                            // var transport = new WebSocketTransport(webSocket, WebSocketTransport.DefaultTransportScheduler, true);
                            // await transport.SendAsync("{\"id\":2, \"method\":\"Runtime.evaluate\", \"params\":{\"expression\": \"document.addEventListener('click', function(e){ alert('injected by CDP333'); }); \"}}");
                        }
                        //动态添加Iframe
                        page.FrameAttached += async (s, ff) =>
                        {
                            Console.WriteLine("new FrameAttached..." + ff.Frame.Url);
                            await ff.Frame.EvaluateExpressionAsync(injectContent);
                        };
                    }
                    
                }
                async void TargetCreatedEventHandler(object sender, TargetChangedArgs e)
                {
                    Page newPage = await e.Target.PageAsync();
                    if (newPage != null)
                    {
                        Console.WriteLine("new target..." + newPage.Url);
                        newPage.Load += async (p, b) =>
                        {
                            await ((Page)p).EvaluateExpressionAsync(injectContent);
                            Console.WriteLine("new page inject js..."+ ((Page)p).Url);
                        };
                        //动态添加Iframe
                        newPage.FrameAttached += async (s, ff) =>
                        {
                            Console.WriteLine("new FrameAttached..." + ff.Frame.Url);
                            await ff.Frame.EvaluateExpressionAsync(injectContent);
                        };
                        // context.TargetCreated -= TargetCreatedEventHandler;
                        await newPage.EvaluateExpressionAsync(injectContent);
                        
                        // 处理新页面包含iframe的情况
                        foreach (Frame frame in newPage.Frames)
                        {
                            Console.WriteLine("new frame2..." + frame.Url);
                            await frame.EvaluateExpressionAsync(injectContent);
                        }
                        var cts = browser.BrowserContexts();
                        //var frameEl = await newPage.WaitForSelectorAsync(".g-recaptcha div iframe");
                    }
                }
                context.TargetCreated += TargetCreatedEventHandler;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private string LoadEmbedScript(string fileName)
        {
            var assembly = Assembly.GetExecutingAssembly();
            var resourceName = $"PuppeteerDemo.scripts.{fileName}";
            Console.WriteLine(resourceName);
            using (var stream = assembly.GetManifestResourceStream(resourceName))
            {
                if (stream == null) throw new ArgumentException(nameof(fileName));

                using (TextReader tr = new StreamReader(stream))
                {
                    return tr.ReadToEnd();
                }
            }
        }
    }
}
